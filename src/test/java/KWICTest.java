import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class KWICTest {
        public ArrayList<String> initialize_test_case() {
            ArrayList<String> test_case = new ArrayList<>();
            test_case.add("is");
            test_case.add("the");
            test_case.add("of");
            test_case.add("and");
            test_case.add("as");
            test_case.add("a");
            test_case.add("but");
            test_case.add("::");
            test_case.add("Descent of Man");
            test_case.add("The Ascent of Man");
            test_case.add("The Old Man and The Sea");
            test_case.add("A Portrait of The Artist As a Young Man");
            test_case.add("A Man is a Man but Bubblesort IS A DOG");
            return test_case;
        }


    @Test
    void printOut() {
        ArrayList<String> test_case = initialize_test_case();
        ArrayList<String> expected = new ArrayList<>();
        expected.add("a portrait of the ARTIST as a young man");
        expected.add("the ASCENT of man");
        expected.add("a man is a man but BUBBLESORT is a dog");
        expected.add("DESCENT of man");
        expected.add("a man is a man but bubblesort is a DOG");
        expected.add("descent of MAN");
        expected.add("the ascent of MAN");
        expected.add("the old MAN and the sea");
        expected.add("a portrait of the artist as a young MAN");
        expected.add("a MAN is a man but bubblesort is a dog");
        expected.add("a man is a MAN but bubblesort is a dog");
        expected.add("the OLD man and the sea");
        expected.add("a PORTRAIT of the artist as a young man");
        expected.add("the old man and the SEA");
        expected.add("a portrait of the artist as a YOUNG man");
        assertEquals(new KWIC().getOutput(test_case), expected);
    }


    @Test
    void getKeywords() {
        ArrayList<String> test_case = initialize_test_case();
        ArrayList<String> expected = new ArrayList<>();
        expected.add("Artist");
        expected.add("Ascent");
        expected.add("Bubblesort");
        expected.add("Descent");
        expected.add("DOG");
        expected.add("Man");
        expected.add("Old");
        expected.add("Portrait");
        expected.add("Sea");
        expected.add("Young");
        assertEquals(new KWIC().getKeywords(test_case), expected);

    }

    @Test
    void testGetTitles() {
        ArrayList<String> test_case = initialize_test_case();
        ArrayList<String> expected = new ArrayList<>();
        expected.add("Descent of Man");
        expected.add("The Ascent of Man");
        expected.add("The Old Man and The Sea");
        expected.add("A Portrait of The Artist As a Young Man");
        expected.add("A Man is a Man but Bubblesort IS A DOG");
        assertEquals(new KWIC().getTitles(test_case), expected);
    }


    @Test
    void testgetIgnoreWords() {
        ArrayList<String> test_case = initialize_test_case();
        ArrayList<String> expected = new ArrayList<>();
        expected.add("is");
        expected.add("the");
        expected.add("of");
        expected.add("and");
        expected.add("as");
        expected.add("a");
        expected.add("but");
        assertEquals(new KWIC().getIgnores(test_case), expected);

    }

}