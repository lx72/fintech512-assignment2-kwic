import java.util.*;

public class KWIC {
    public static ArrayList<String> getTitles(ArrayList<String> input) {
        ArrayList<String> titles = new ArrayList<>();
        int pos = 0;
        String delimiter = "::";
        while ((input.get(pos)).equals(delimiter) == false) {
            pos++;
        }
        pos++;
        for (int i = pos; i < input.size(); i++) {
            titles.add(input.get(i));
        }
        return titles;
    }

    public static ArrayList<String> getIgnores(ArrayList<String> input) {
        ArrayList<String> ignores = new ArrayList<>();
        int pos = 0;
        String delimiter = "::";
        while ((input.get(pos)).equals(delimiter) == false) {
            ignores.add(input.get(pos));
            pos++;
        }
        return ignores;
    }

    public static ArrayList<String> getKeywords(ArrayList<String> input) {
        ArrayList<String> ignores = getIgnores(input);
        ArrayList<String> titles = getTitles(input);
        ArrayList<String> keywords = new ArrayList<>();
        for (int i = 0; i < titles.size(); i++) {
            String title = titles.get(i);
            for (String j : title.split(" ")) {
                keywords.add(j);
            }
        }
        for (int i = 0; i < ignores.size(); i++) {
            for (int j = 0; j < keywords.size(); j++) {
                if (ignores.get(i).compareToIgnoreCase(keywords.get(j)) == 0) {
                    keywords.remove(j);
                    j--;
                }
            }
        }
        HashSet<String> temp = new HashSet<>();
        temp.addAll(keywords);
        keywords.clear();
        keywords.addAll(temp);
        keywords.sort(String::compareToIgnoreCase);
        return keywords;
    }

    public static ArrayList<String> getOutput(ArrayList<String> input) {
        ArrayList<String> keywords = getKeywords(input);
        ArrayList<String> titles = getTitles(input);
        ArrayList<String> output = new ArrayList<>();
        for (int i = 0; i < keywords.size(); i++) {
            for (int j = 0; j < titles.size(); j++) {
                if (titles.get(j).contains(keywords.get(i))) {
                    String[] temp = titles.get(j).split(" ");
                    String str_output = "";
                    int count_keyword = 0;
                    for (int k = 0; k < temp.length; k++) {
                        if (temp[k].compareToIgnoreCase(keywords.get(i)) == 0) {
                            count_keyword++;
                        }
                    }
                    for (int k = 0; k < temp.length; k++) {
                        if (temp[k].compareToIgnoreCase(keywords.get(i)) != 0) {
                            str_output += temp[k].toLowerCase() + " ";
                        } else {
                            str_output += temp[k].toUpperCase() + " ";
                        }
                    }
                    str_output = str_output.substring(0, str_output.length() - 1);
                    if (count_keyword == 1) {
                        output.add(str_output);
                    } else {
                        int count_output = 0;
                        while (count_output != count_keyword) {
                            String[] temp_output = str_output.split(" ");
                            str_output = "";
                            for (int m = 0; m < temp_output.length; m++) {
                                temp_output[m] = temp_output[m].toLowerCase();
                            }
                            int appearance = 0;
                            for (int m = 0; m < temp_output.length; m++) {
                                if (temp_output[m].compareToIgnoreCase(keywords.get(i)) == 0) {
                                    appearance++;
                                    if (appearance - 1 == count_output) {
                                        temp_output[m] = temp_output[m].toUpperCase();
                                        break;
                                    }
                                }
                            }
                            for (int m = 0; m < temp_output.length; m++) {
                                str_output += temp_output[m] + " ";
                            }
                            str_output = str_output.substring(0, str_output.length() - 1);
                            output.add(str_output);
                            count_output++;
                        }
                    }
                }
            }
        }
        return output;
    }

    public static void main(String[] args) {
        ArrayList<String> input = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            input.add(scanner.nextLine());
        }
        ArrayList<String> output = getOutput(input);
        for (int i = 0; i < output.size(); i++) {
            System.out.println(output.get(i));
        }
    }
}
